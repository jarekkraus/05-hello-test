package uj.jwzp2019.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import uj.jwzp2019.model.Person;
import uj.jwzp2019.service.PeopleService;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@ExtendWith(MockitoExtension.class)
public class SaveControllerTest {

    private MockMvc mockMvc;

    @Mock
    private PeopleService peopleService;

    @InjectMocks
    public SaveController saveController;

    @BeforeEach
    private void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(saveController)
                .build();
    }

    @Test
    void getPersonByIdReturnsPersonForDefaultId() throws Exception {
        // given
        Person han = new Person();
        han.setName("Han Solo");
        given(peopleService.getPersonById(1)).willReturn(han);
        // when
        MockHttpServletResponse response = mockMvc.perform(
                get("/save/")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();
        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("Ok");

    }

    @Test
    void getPersonByIdReturnsPersonWithPinkEyeColor() throws Exception {
        // given
        String params = "id=1";
        Person han = new Person();
        han.setName("Han Solo");
        given(peopleService.getPersonById(1)).willReturn(han);
        // when
        MockHttpServletResponse response = mockMvc.perform(
                get("/save?" + params)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();
        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("Ok");
        assertThat(saveController.getPerson().getEye_color().equals("pink"));
    }

    @Test
    void setPrefixWithoutParameter() throws Exception {
        // given
        String params = "";
        // when
        MockHttpServletResponse response = mockMvc.perform(
                get("/prefix" + params)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();
        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    void setPrefixWithParameter() throws Exception {
        // given
        String params = "prefix=file";
        // when
        MockHttpServletResponse response = mockMvc.perform(
                get("/prefix?" + params)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();
        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(saveController.getPREFIX().equals(params));
    }

}
