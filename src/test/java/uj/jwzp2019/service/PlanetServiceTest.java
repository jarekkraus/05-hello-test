package uj.jwzp2019.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;
import uj.jwzp2019.model.Planet;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class PlanetServiceTest {

    private static final String FAKE_URL_API = "http://fake.url/api/";
    private static final int ID = 1;
    private static final String TATOOINE = "Tatooine";

    @Mock
    private RestTemplate restTemplate;

    @Test
    void getPlanetByUrlReturnsPlanet() {
        // given
        Planet planet = new Planet();
        planet.setName(TATOOINE);
        PlanetService planetService = new PlanetService(FAKE_URL_API, restTemplate);
        given(restTemplate.getForObject(FAKE_URL_API + "planets/" + ID, Planet.class)).willReturn(planet);
        // when
        Planet testPlanet = planetService.getPlanetByUrl(FAKE_URL_API  + "planets/" + ID);
        // then
        assertThat(testPlanet.getName()).isEqualTo(TATOOINE);

    }
}
