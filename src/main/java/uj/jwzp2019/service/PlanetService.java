package uj.jwzp2019.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uj.jwzp2019.model.Planet;

@Service
public class PlanetService {

    private final String starWarsApiUrl;
    private final RestTemplate restTemplate;

    @Autowired
    public PlanetService(@Value("${starwars.api.url}") String starWarsApiUrl, RestTemplate restTemplate) {
        this.starWarsApiUrl = starWarsApiUrl;
        this.restTemplate = restTemplate;
    }

    public Planet getPlanetByUrl(String url) {
        Planet result = restTemplate.getForObject(url, Planet.class);
        return result;
    }
}
