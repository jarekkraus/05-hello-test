package uj.jwzp2019.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uj.jwzp2019.model.Person;
import uj.jwzp2019.model.Planet;


@Service
public class PeopleService {

    private final String starWarsApiUrl;
    private final RestTemplate restTemplate;
    private final PlanetService planetService;

    @Autowired
    public PeopleService(@Value("${starwars.api.url}") String starWarsApiUrl, RestTemplate restTemplate, PlanetService planetService) {
        this.starWarsApiUrl = starWarsApiUrl;
        this.restTemplate = restTemplate;
        this.planetService = planetService;
    }

    public Person getPersonById(int id) {
        Person result = restTemplate.getForObject(starWarsApiUrl + "people/" + id, Person.class);
        if (result != null) {
            Planet planet = planetService.getPlanetByUrl(result.getHomeworld());
            result.setPlanet(planet);
        }
        return result;
    }
}
